//
//  GameScene.swift
//  SpriteKitIntro
//
//  Created by MacStudent on 2019-02-06.bjbjhjh
//  Copyright © 2019 MacStudent. All rights r gffgdgfdfggfdgffggffgfggfgfeservedvv.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {

    // Example 1  - Adding text to the screen
    let label = SKLabelNode(text:"HELLO WORLD!")
    let label2 = SKLabelNode(text:"ABCD")
    
    // Example 2 - Draw a square on the screen
    let square = SKSpriteNode(color: SKColor.blue, size: CGSize(width: 50, height: 50))
    
    // Example 3 - Draw an image on the screen
    let duck = SKSpriteNode(imageNamed: "psyduck")
    
    // Example 4 - Draw a circle on the screen
    let circle = SKShapeNode(circleOfRadius: 40)
    
    
    override func update(_ currentTime: TimeInterval) {
          //  print("time function\(currentTime)")
    }
    override func didMove(to view: SKView) {
        // output the size of the screen
        print("Screen size (w,h): \(size.width),\(size.height)")
        
        // Add images to the scene
        let bug = SKSpriteNode(imageNamed: "caterpie")
        bug.position = CGPoint(x:size.width/2, y:size.height/2)
        addChild(bug)
        
        duck.position = CGPoint(x:size.width/2+100, y:size.height/2)
        addChild(duck)
        
        
        // configure your text
        label.position = CGPoint(x:size.width/2, y:size.height/2)
        label.fontSize = 45
        label.fontColor = SKColor.yellow

        label2.fontSize = 60
        label2.position = CGPoint(x:size.width/2, y:200)
        
        // add it to your scene (draw it!)
        addChild(label)
        addChild(label2)
        
        // configure the square
        square.position = CGPoint(x: 105, y:700);
        // add square to scene
        addChild(square)
        
        // configure your circle
        // -----------------------
        // color of border
        circle.strokeColor = SKColor.yellow
        // width of border
        circle.lineWidth = 5
        // fill color
        circle.fillColor = SKColor.magenta
        // location of circle
        circle.position = CGPoint(x:200, y:100)
        addChild(circle)
        
        //movement-->SK-ACTION
        
        //1.Single movement
        //2. Group of movements
        
        //1) Define a movement
        
        
        //moveBy() ->move specifi distance each time
        let moveAction = SKAction.moveBy(x: -50, y: 0, duration: 2)
        let moveAction1 = SKAction.moveBy(x: 0, y: 600, duration: 2)
        
        let sequence:SKAction = SKAction.sequence([moveAction, moveAction1])
       // circle.run(sequence.reversed())
       // duck.run(SKAction.repeatForever(sequence))
        duck.run(SKAction.repeat(sequence, count: 5))
        let moveAction2 = SKAction.moveTo(x: 300, duration: 2)
          let newPosition = CGPoint(x:300,y:700);
         let moveAction3 = SKAction.move(to: newPosition, duration: 2)
        let sequence2:SKAction = SKAction.sequence([moveAction1,moveAction3])
        //square.run(sequence2)
        
        //move() and moveTo() -> move to a specific coordinate on the screen
      
       square.run(moveAction1.reversed())
        //2) Apply the movement to the character
        //circle.run(moveAction.reversed())
        //circle.run(moveAction3)
        
        circle.run(moveAction)
        //Check circle touches the rectangle
        
        if(circle.frame.intersects(square.frame))
        {
            print("collision detected")
        }
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
            let location = touch.location(in: self)
            let node = atPoint(location)
            
            if(location.x < self.size.width/2)
            {
                print("left touch")
            }
            if(location.x >= self.size.width/2)
                {
                    print("right touch")
                }
                
            
        }
    }
    
}
